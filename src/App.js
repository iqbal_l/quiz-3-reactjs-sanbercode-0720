import React from 'react';
import logo from './logo.svg';
import './App.css';
import './kuis3/public/css/style.css'
import {BrowserRouter} from "react-router-dom"
import Cookies from 'js-cookie'

import {LoginProvider} from './kuis3/context/LoginContext'
import Menu from './kuis3/navigasi/Routes'

function App() {
  const get_access = Cookies.get('access_token')

  return (
    <LoginProvider>
      <BrowserRouter>
        <Menu />
      </BrowserRouter>
    </LoginProvider>

  );
}

export default App;
