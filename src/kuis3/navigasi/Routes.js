import React, {useContext} from "react";
import { Switch, Link, Route, Redirect } from "react-router-dom";
import logo from '../public/img/logo.png';
import Home from '../Index';
import About from '../About';
import Movie from '../Movie';
import Login from '../Login';

import Cookies from 'js-cookie'


import {LoginContext, LoginLogoutContext} from "../context/LoginContext"

const Routes = () => {
  const [isLogin, setLogin] = useContext(LoginContext);
  const [handleLogout] = useContext(LoginLogoutContext);


  return (
    <>
    <header>
      <img id="logo" src={logo} width="200px" />
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About</Link></li>
          {isLogin == '1' ? (
            <>
            <li><Link to="/movie-list">Movie List Editor</Link></li>
            <li><a onClick={handleLogout} href = "/">Logout</a></li>
            </>
          ) : (
            <li><Link to="/login">Login</Link></li>
          )}
        </ul>
      </nav>
  </header>
  <Switch>
    <Route path="/login" component={Login} />
    <Route path="/movie-list" component={Movie} />
    <Route path="/about" component={About} />
    <Route path="/" component={Home} />
  </Switch>
  <footer>
    <h5>copyright &copy; 2020 by Sanbercode</h5>
  </footer>
    </>
  );
};

export default Routes;
