import React, {useContext} from 'react'

import Cookies from 'js-cookie'
import LoginForm from './login/LoginForm'

const Login = () => {
  const access_token = Cookies.get('access_token')
  return (

    <>
    {
      (access_token === '0')?
        <LoginForm />
      :  <section><div id="article-list"><h2>Anda sudah Login</h2></div></section>
    }
    </>
  )
}

export default Login
