<html>
  <head>
    <link href="public/css/style.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
  </head>
  <body>
    <section>
    <div class='container-kontak'>
      <h1>Hubungi Kami</h1>
      <div class='hubungi-kami'>
        <ol>
          <li><b>Kantor :</b> di jalan belum jadi</li>
          <li><b>Nomor Telepon :</b> 08XX-XXXX-XXXX</li>
          <li><b>Email :</b>email@contoh.com</li>
        </ol>
      </div>

      <h1>Kirimkan Pesan</h1>
      <div class='hubungi-kami'>
        <ol>
          <li>
            <form action="index.html" method="get">
            <table width="100%">
              <tr>
                <td width="15%"><b>Nama</b></td>
                <td><input type="text" name="nama" size="10"></td>
              </tr>
              <tr>
                <td><b>Email</b></td>
                <td><input type="text" name="email" size="10"></td>
              </tr>
              <tr>
                <td><b>Jenis Kelamin</b></td>
                <td>
                  <input type="radio" value="Laki-laki" name="jeniskelamin" size="10" checked> Laki-laki<br>
                  <input type="radio" value="Perempuan" name="jeniskelamin" size="10"> Perempuan
                </td>
              </tr>
              <tr>
                <td><b>Pesan Anda</b></td>
                <td><textarea name="pesan" cols="50" rows="4"></textarea></td>
              </tr>
              <tr>
                <td><button type="submit">Kirim</button></td>
                <td></td>
              </tr>
            </table>
            </form>
          </li>
        </ol>
      </div>
      <a href="index.html">Kembali Ke Index</a>
    </section>
  </body>
</html>
