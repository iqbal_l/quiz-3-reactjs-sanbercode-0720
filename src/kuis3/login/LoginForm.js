import React, {useContext} from 'react'
import {LoginContext, LoginChangeContext, LoginSubmitContext, LoginWarningContext, LoginGagalContext} from '../context/LoginContext'

const LoginForm = () => {
  const [handLeLogin] = useContext(LoginSubmitContext)
  const [handleChange] = useContext(LoginChangeContext)
  const [warning, setWarning] = useContext(LoginWarningContext)
  const [isGagal, setGagal] = useContext(LoginGagalContext)

  return (
    <>
    <section>
    <div class='container-login'>
      <h1>Login</h1>
      <div class='data-login'>
      <h4 style={{color:"red"}} align="center">{warning}</h4>
        <div style={{margin:"0 auto",height:300,display: "flex",justifyContent:"center"}}>

            <form onSubmit={handLeLogin} style={{textAlign:"center"}}>
              <table>
                <tbody>
                <tr>
                  <td style={{textAlign:"left"}}>Username/Email</td>
                  <td>
                    <input name='user' onChange={handleChange} size="40" placeholder="Username/Email"/>
                    <br/>
                    <span style={{color:'red'}}>{isGagal.user}</span>
                  </td>
                </tr>
                <tr>
                  <td style={{textAlign:"left"}}>Password</td>
                  <td><input name='password' onChange={handleChange} size="40" placeholder="Password"/>
                  <br/>
                    <span style={{color:'red'}}>{isGagal.password}</span>
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><button type="submit" >Login</button></td>
                </tr>
                </tbody>
              </table>
            </form>

        </div>
      </div>
    </div>
    </section>
    </>
  )
}

export default LoginForm
