mport React, {useContext} from 'react'
import {MovieContext} from '../context/MovieContext'

const ListMovie = () => {
    const [movie, setMovie] = useContext(MovieContext)

    return (
      <>
      {
        movie !== null && movie.map((item, index) => {
          return (
            <div key={index}>
              <a href=""><h3>{item.title}</h3></a>
              <p>
                <b>Rating {item.rating}</b>
                <br/>
                <b>Durasi: {item.duration}</b>
                <br/>
                <b>genre: {item.genre}</b>
                <br/>
              </p>
              <p>
                <b>description:</b> {item.description}
              </p>
            </div>
          )
       })
      }
      </>
    )

}

export default ListMovie
