import React, {Component, useContext} from 'react'
import {LoginContext} from './context/LoginContext'
import {MovieProvider} from './context/MovieContext'
import ListMovie from './movie/ListMovie'
import FormMovie from './movie/FormMovie'

import Cookies from 'js-cookie'

const Movie = () => {
  const [isLogin, setLogin] = useContext(LoginContext)


    return (
      <>
        <section>
      {
      (isLogin === '1')?(

        <div id="article-list">
            <MovieProvider>
              <ListMovie />
              <FormMovie />
            </MovieProvider>
          </div>

        ):(<div id="article-list"><h2>Anda belum Login</h2></div>)

      }
        </section>
      </>
    )

}

export default Movie
