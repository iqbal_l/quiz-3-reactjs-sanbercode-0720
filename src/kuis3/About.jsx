import React from 'react'
import { Link, Switch, Route} from "react-router-dom";

import Home from './Index'

const About = () => {

  return (
    <>
    <section>
    <div class='container-about'>
      <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <div class='data-peserta'>
        <ol>
          <li><b>Nama :</b> M Iqbal Laksana</li>
          <li><b>Email :</b> iqballaksana01@gmail.com</li>
          <li><b>Sistem Operasi yang digunakan :</b>Windows 10</li>
          <li><b>Akun Gitlab :</b> @iqbal_l</li>
          <li><b>Akun Telegram :</b> @iqbal_l</li>
        </ol>
      </div>
    </div>
    </section>
    </>
  )
}

export default About
