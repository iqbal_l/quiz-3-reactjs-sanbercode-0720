import React, {Component} from 'react'
import {MovieProvider} from './context/MovieContext'
import ListMovie from './home/ListMovie'

class Home extends Component {
  render() {
    return (
      <>
      <section>
        <h1>Daftar Film - Film Terbaik</h1>
        <div id="article-list">
          <MovieProvider>
            <ListMovie />
          </MovieProvider>
        </div>
      </section>

      </>
    )
  }
}

export default Home
