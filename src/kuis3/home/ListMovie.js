import React, {useContext} from 'react'
import {MovieContext} from '../context/MovieContext'

const ListMovie = () => {
    const [movie, setMovie] = useContext(MovieContext)

    const sorting = () =>{
      movie.sort((a,b) => (a.rating > b.rating) ? -1 : ((b.rating > a.rating) ? 1 : 0));
    }

    return (
      <>
      {sorting()}
      {

        movie !== null && movie.map((item, index) => {
          return (
            <div key={index}>
              <a href=""><h3>{item.title}</h3></a>
              <p>
                <b>Rating {item.rating}</b>
                <br/>
                <b>Durasi: {Math.round(item.duration/60)} jam</b>
                <br/>
                <b>genre: {item.genre}</b>
                <br/>
              </p>
              <p>
                <b>description:</b> {item.description}
              </p>
            </div>
          )
       })
      }
      </>
    )

}

export default ListMovie
