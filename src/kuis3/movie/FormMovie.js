import React, {useContext, useState} from "react"
import {MovieContext, MovieSelectedContext, MovieStatusFormContext, MovieInputContext, MovieChangeContext} from "../context/MovieContext"
import axios from 'axios';

const FormMovie = () =>{
  const [movie, setMovie] = useContext(MovieContext)
  const [input, setInput] = useContext(MovieInputContext)
  const [selectedId, setSelectedId]  =  useContext(MovieSelectedContext)
  const [statusForm, setStatusForm]  =  useContext(MovieStatusFormContext)
  const [handleChange]  =  useContext(MovieChangeContext)
  const [warning,setWarning]  =  useState({title:'',description:'',rating:'',genre:'',duration:'',year:''})

  const handleSubmit = (event) => {
    event.preventDefault()

    let title = input.title;
    let description = input.description;
    let rating = input.rating;
    let genre = input.genre;
    let duration = input.duration
    let year = input.year

    let cekTitle = '';
    let cekDescription = '';
    let cekDuration = '';
    let cekRating = '';
    let cekYear = '';

    if (title.replace(/\s/g,'') === ""){

    }

    if (title.replace(/\s/g,'') === ""){
      cekTitle = 'Title wajib diisi'
    }

    if (description.replace(/\s/g,'') === ""){
      cekDescription = 'Description wajib diisi'
    }

    if (isNaN(rating) === true){
      cekRating = "Rating harus angka"
    }else{
      rating = parseInt(rating)
      if (rating < 1 || rating > 10){
        cekRating = "Rating harus dalam range 1 sampai 10"
      }
    }

    if (isNaN(year) === true){
      cekYear = "Year harus angka"
    }

    if (isNaN(duration) === true){
      cekDuration = "Duration harus angka"
    }

    setWarning({
      title:cekTitle,
      description:cekDescription,
      rating:cekRating,
      duration:cekDuration,
      genre:'',
      year:cekYear
    })

    if (title.replace(/\s/g,'') !== "" && description.toString().replace(/\s/g,'') !== "" && isNaN(rating) === false ) {
      if (rating >= 1 && rating <= 10){
        if (statusForm === "create"){
          axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title,description,rating,genre,duration,year})
            .then(res => {
              setMovie([...movie, {id: res.data.id, title:title,description:description,rating:rating,genre:genre,duration:duration,year:year}])
            })
        }else if(statusForm === "edit"){
          axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title,description,rating,genre,duration,year})
          .then(res =>{
              let newDaftarMovie = movie.find(el=> el.id === selectedId)
              newDaftarMovie.title = title
              newDaftarMovie.description = description
              newDaftarMovie.rating = rating
              newDaftarMovie.duration = duration
              newDaftarMovie.genre = genre
              newDaftarMovie.year = year
              setMovie([...movie])
          })
        }


      setStatusForm("create")
      setSelectedId(0)
      setInput({
        title:'',
        description:'',
        rating:'0',
        duration:'0',
        genre:'',
        year:''
      })

      setWarning({
        title:'',
        description:'',
        rating:'',
        duration:'',
        genre:'',
        year:''
      })
      }
    }


  }

  return(
    <>
      <h1>Form Daftar Movie</h1>
      <form onSubmit={handleSubmit}>
      <table>
        <tbody>
        <tr>
          <td><span style={{color:"red"}}>*</span>Title</td>
          <td><input name="title" type="text" value={input.title} onChange={handleChange}/><br/>
          <span style={{color:"red"}}>{warning.title}</span></td>
          <td></td>
          <td><span style={{color:"black"}}>**</span>Rating</td>
          <td>
            <input name="rating" type="text" value={input.rating} onChange={handleChange}/> 1-10<br/>
            <span style={{color:"red"}}>{warning.rating}</span>
          </td>
        </tr>
        <tr>
          <td rowspan="1"><span style={{color:"red"}}>*</span>Description</td>
          <td rowspan="1"><textarea name="description"  onChange={handleChange} value={input.description}>{input.description}</textarea><br/>
          <span style={{color:"red"}}>{warning.description}</span></td>
          <td></td>
          <td><span style={{color:"black"}}>**</span>Duration</td>
          <td><input name="duration" type="text" value={input.duration} onChange={handleChange}/> menit<br/>
          <span style={{color:"red"}}>{warning.duration}</span></td>
        </tr>
        <tr>
          <td rowspan="1">Year</td>
          <td rowspan="1"><input text="type" name="year"  onChange={handleChange} value={input.year}/ ><br/>
          <span style={{color:"red"}}>{warning.year}</span></td>
          <td></td>
          <td>Genre</td>
          <td><input name="genre" type="text" value={input.genre} onChange={handleChange}/></td>
        </tr>
        <tr>
          <td style={{paddingLeft:"0px"}}>
          <span style={{color:"red",fontSize:10}}>*wajib diisi</span><br/>
          <span style={{color:"black",fontSize:10}}>**angka saja</span><br/>
          <button>submit</button>
          </td>
        </tr>

        </tbody>
        </table>
      </form>
    </>
  )

}

export default FormMovie
