import React, {useContext} from 'react'
import {MovieContext, MovieHapusContext, MovieEditContext} from '../context/MovieContext'

const ListMovie = () => {
    const [movie, setMovie] = useContext(MovieContext)
    const [handleHapus] = useContext(MovieHapusContext)
    const [handleEdit] = useContext(MovieEditContext)

    return (
      <>
      <h1 align="center">Pengaturan Movie</h1>
      <table style={{border:"1px solid black"}} width="100%">
      <thead>
        <tr>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>No</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Title</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Description</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Rating</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Duration</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Genre</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Aksi</th>
        </tr>
        </thead>
        <tbody>
        {

             movie !== null && movie.map((val, index)=>{
               return(
                 <tr key={val.id}>
                   <td style={{backgroundColor:"#FF7F50"}}>{index+1}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.title}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.description}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.rating}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{Math.round(val.duration/60)} jam</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.genre}</td>
                   <td style={{textAlign:"center"}}>
                      <button onClick={handleEdit} value={val.id}>Ubah</button>
                      &nbsp;
                      <button onClick={handleHapus} value={val.id}>Delete</button>
                  </td>
                 </tr>
               )
             })
         }
        </tbody>
      </table>
      </>
    )

}

export default ListMovie
